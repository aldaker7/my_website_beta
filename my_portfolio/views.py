from django.shortcuts import render
from django.core.mail import send_mail
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse
import smtplib
import os.path
import wget
import os
import webbrowser


# Create your views here.


def get_home_page(request):
    return render(request,'index.html',context={})


def send_email(request):
    if request.method == 'POST':
        server = smtplib.SMTP('smtp.gmail.com', 587)
        header = 'From: %s' % request.POST.get('contactEmail')
        header += 'To: %s' % 'ahmad.aldaker7@gmail.com'
        header += 'Subject: %s' % request.POST.get('contactSubject')
        message = "Name: "+request.POST.get('contactName')+'\n'+request.POST.get('contactMessage')
        server.starttls()
        server.login('your_email','your password')
        contact_me = server.sendmail(request.POST.get('contactEmail'), 'ahmad.aldaker7@gmail.com', message)
        server.quit()

        return HttpResponseRedirect(reverse('home'))
    else:
        return HttpResponseRedirect(reverse('home'))


def download_cv(request):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    doc_dir = os.path.join(os.path.join(os.path.join(BASE_DIR, "my_portfolio"), 'static'), 'document')
    if os.name == 'nt':
        location = os.path.expanduser('~\\Downloads\\Aldaker_CV.doc')
        os.rename(doc_dir + "\\Aldaker_latest_cv.doc", location)

    else:
        location = os.path.expanduser('~/Downloads/Aldaker_CV.doc')
        os.rename(doc_dir + "/Aldaker_latest_cv.doc", location)


    webbrowser.open('file:///%s' % location)
    return HttpResponseRedirect(reverse('home'))